**2019年3月24日周报**

### 1.本周制定的工作目标有哪些?
##### 对论文整体进行修改，按照讨论的内容修改论文
### 2.哪些工作目标完成了？感觉完成地怎么样？
##### 基本完成论文的修改，但结论部分及一些细节仍需修改
### 3.哪些工作目标没有完成？下周需要做哪些努力?
##### 下周需要将结论部分继续修改
### 4.阅读了哪些文献？
##### 里程计辅助车载GNSS_INS组合导航性能分析
##### 车载移动测量系统点云误差分析及修正
##### PS_MEMS INS integrated system for navigation in urban areas[J]. Gps Solutions
### 5.本周工作成果总结，说说你对自己点赞或失望的地方
##### 本周基本完成任务，但仍有不足
### 6.有遇到挑战或者困难吗？希望团队怎样帮助你？需要老师指导吗？
##### 需要多查阅相关文献，多增加一些新的文献
