#include "gps_dw.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


void GPS::n_in(string Nwenjian)
{


  string line;
  string line1;double a;
  ifstream open(Nwenjian,ios::in);
  if(!open)
  {
      cerr<<"not open"<<endl;
      exit(EXIT_FAILURE);
  }

while(line.find("END OF HEADER")== string::npos)

  {   getline(open,line);

}

       while(!open.eof())
{


       getline(open,line);

       if (open.fail())
       break;

       line1=line.substr(0,2);
       a=atof(line1.c_str());
      N_PRN.push_back(a);

       line1=line.substr(3,2);
       a=atof(line1.c_str());
       N_year.push_back(a);

       line1=line.substr(6,2);
       a=atof(line1.c_str());
       N_month.push_back(a);

       line1=line.substr(9,2);
       a=atof(line1.c_str());
       N_day.push_back(a);

       line1=line.substr(12,2);
       a=atof(line1.c_str());
      N_hour.push_back(a);

       line1=line.substr(15,2);
       a=atof(line1.c_str());
       N_minute.push_back(a);


       line1=line.substr(18,4);
       a=atof(line1.c_str());
        N_second.push_back(a);


       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_wspc.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
        N_wspy.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
        N_wspysd.push_back(a);


       getline(open,line);


       line1=line.substr(3,19);
       a=atof(line1.c_str());
        N_IODE.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_crs.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
        N_n.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
        N_M0.push_back(a);

       getline(open,line);

       line1=line.substr(3,19);
       a=atof(line1.c_str());
       N_cuc.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
       N_e.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
        N_cus.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
      N_sqrta.push_back(a);


       getline(open,line);

       line1=line.substr(3,19);
       a=atof(line1.c_str());
        N_TOE.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_cic.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
       N_OMEGA.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
      N_cis.push_back(a);


       getline(open,line);


       line1=line.substr(3,19);
       a=atof(line1.c_str());
        N_i0.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_crc.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
       N_w.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
       N_OMEGADOT.push_back(a);

       getline(open,line);

       line1=line.substr(3,19);
       a=atof(line1.c_str());
       N_IDOT.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_L2.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
        N_zs.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
        N_L2P.push_back(a);

       getline(open,line);

       line1=line.substr(3,19);
       a=atof(line1.c_str());
        N_m.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
        N_wszt.push_back(a);

       line1=line.substr(41,19);
       a=atof(line1.c_str());
        N_TGD.push_back(a);

       line1=line.substr(60,19);
       a=atof(line1.c_str());
       N_IODC.push_back(a);

       getline(open,line);

       line1=line.substr(3,19);
       a=atof(line1.c_str());
        N_wsfs.push_back(a);

       line1=line.substr(22,19);
       a=atof(line1.c_str());
      N_nhqj.push_back(a);


   }
open.close();

}





void GPS::o_in(string Owenjian)
{
   ifstream infile(Owenjian,ios::in);
   if(!infile){
       cerr<<"open error!"<<endl;
       exit(1);
   }
   string line,APPROX_X1,APPROX_Y1,APPROX_Z1,num_leixing1,leixing2;
       do{
           getline(infile, line);
           if(line.find("APPROX POSITION XYZ")!= string::npos){
           APPROX_X1=line.substr(1,13);
           APPROX_Y1=line.substr(15,13);
           APPROX_Z1=line.substr(29,13);
           APPROX_X=atof(APPROX_X1.c_str());
           APPROX_Y=atof(APPROX_Y1.c_str());
           APPROX_Z=atof(APPROX_Z1.c_str());
           }
           else if(line.find("# / TYPES OF OBSERV")!= string::npos){
               num_leixing1=line.substr(5,1);
               GCZSL=atoi(num_leixing1.c_str());
               for(int j=0;j<GCZSL;j++){
                      leixing2=line.substr(10+j*6,2);
                      O_GCZLX.push_back(leixing2);
               }
           }
       }while(line.find("END OF HEADER") == string::npos);

   for(int jj=0;jj< GCZSL;jj++){
       if(O_GCZLX[jj]=="C1")
           C1=jj;
       else if(O_GCZLX[jj]=="P2")
           P2=jj;
       else if(O_GCZLX[jj]=="L1")
           L1=jj;
       else if(O_GCZLX[jj]=="L2")
           L2=jj;
   }
       string GCZ1,GCZ3,GCZ5,LX1,LX2;
       int sum,sum1,sum2;
       double GCZ2;
       int GCZ4,SL;
       O_GCZ.resize(GCZSL);
       while(!infile.eof()){
              getline(infile,line);
              if(infile.fail())
              break;
              GCZ1=line.substr(1,2);
              GCZ2=atof(GCZ1.c_str());
              O_year.push_back(GCZ2);
              GCZ1=line.substr(4,2);
              GCZ2=atof(GCZ1.c_str());
              O_month.push_back(GCZ2);
              GCZ1=line.substr(7,2);
              GCZ2=atof(GCZ1.c_str());
              O_day.push_back(GCZ2);
              GCZ1=line.substr(10,2);
              GCZ2=atof(GCZ1.c_str());
              O_hour.push_back(GCZ2);
              GCZ1=line.substr(13,2);
              GCZ2=atof(GCZ1.c_str());
              O_minute.push_back(GCZ2);
              GCZ1=line.substr(16,10);
              GCZ2=atof(GCZ1.c_str());
              O_second.push_back(GCZ2);
              GCZ1=line.substr(28,1);
              GCZ2=atof(GCZ1.c_str());
              O_LYBZ.push_back(GCZ2);
              GCZ1=line.substr(30,2);
              GCZ4=atoi(GCZ1.c_str());
              SL=GCZ4;
              sum1=0;
              sum2=0;
              sum=0;
              if(SL<=12){
                for(int i=0;i<SL;i++){
                GCZ3=line.substr(32+3*i,3);
                LX1=GCZ3.substr(0,1);
                if(LX1=="G"){
                   O_GCWXS.push_back(GCZ3);
                sum1+=1;
                }
                }
              }
              else{
                for(int i=0;i<12;i++){
                GCZ3=line.substr(32+3*i,3);
                LX1=GCZ3.substr(0,1);
                if(LX1=="G"){
                    O_GCWXS.push_back(GCZ3);
                sum1+=1;
                }
                }
                getline(infile,line);
                for(int i=0;i<SL-12;i++){
                GCZ5=line.substr(32+3*i,3);
                LX2=GCZ5.substr(0,1);
                if(LX2=="G"){
                   O_GCWXS.push_back(GCZ5);
                sum2+=1;
                }
                }
              }
              sum=sum1+sum2;
              O_GPSGS.push_back(sum);
           if(GCZSL<=5){
              for(int m=0;m<sum;m++){
                  getline(infile,line);
                      for(int n=0;n<GCZSL;n++){
                          GCZ1=line.substr(n*16,16);
                          GCZ2=atof(GCZ1.c_str());
                          O_GCZ[n].push_back(GCZ2);
                      }
              }
              for(int n=0;n<SL-sum;n++)
                  getline(infile,line);

              }
              else{
                   for(int m=0;m<sum;m++){
                       getline(infile,line);
                           for(int n=0;n<5;n++){
                               GCZ1=line.substr(n*16,16);
                               GCZ2=atof(GCZ1.c_str());
                               O_GCZ[n].push_back(GCZ2);
                           }
                         getline(infile,line);
                         for(int n=0;n<GCZSL-5;n++){
                             GCZ1=line.substr(n*16,16);
                             GCZ2=atof(GCZ1.c_str());
                             O_GCZ[n+5].push_back(GCZ2);
                          }
                   }
                   for(int n=0;n<2*(SL-sum);n++)
                       getline(infile,line);
               }
       }
       string prn1;
       double prn;
       int sss=0;
       O_PRN.resize(O_year.size());
       for(int ll=0;ll<O_year.size();ll++){
           if(ll>0)
           sss+=O_GPSGS[ll-1];
       for(int kk=0;kk<O_GPSGS[ll];kk++){
           prn1=O_GCWXS[kk+sss].substr(1,2);
           prn=atof(prn1.c_str());
           O_PRN[ll].push_back(prn);//鍗槦PRN
           }
       }
       infile.close();
}


void GPS::o_in()
{
   ifstream infile("E://O//ww.11o",ios::in);
   if(!infile){
       cerr<<"open error!"<<endl;
       exit(1);
   }
   string line,APPROX_X1,APPROX_Y1,APPROX_Z1,num_leixing1,leixing2;
       do{
           getline(infile, line);
           if(line.find("APPROX POSITION XYZ")!= string::npos){
           APPROX_X1=line.substr(1,13);
           APPROX_Y1=line.substr(15,13);
           APPROX_Z1=line.substr(29,13);
           APPROX_X=atof(APPROX_X1.c_str());
           APPROX_Y=atof(APPROX_Y1.c_str());
           APPROX_Z=atof(APPROX_Z1.c_str());
           }
           else if(line.find("# / TYPES OF OBSERV")!= string::npos){
               num_leixing1=line.substr(5,1);
               GCZSL=atoi(num_leixing1.c_str());
               for(int j=0;j<GCZSL;j++){
                      leixing2=line.substr(10+j*6,2);
                      O_GCZLX.push_back(leixing2);
               }
           }
       }while(line.find("END OF HEADER") == string::npos);

   for(int jj=0;jj< GCZSL;jj++){
       if(O_GCZLX[jj]=="C1")
           C1=jj;
       else if(O_GCZLX[jj]=="P2")
           P2=jj;
       else if(O_GCZLX[jj]=="L1")
           L1=jj;
       else if(O_GCZLX[jj]=="L2")
           L2=jj;
   }
       string GCZ1,GCZ3,GCZ5,LX1,LX2;
       int sum,sum1,sum2;
       double GCZ2;
       int GCZ4,SL;
       O_GCZ.resize(GCZSL);
       while(!infile.eof()){
              getline(infile,line);
              if(infile.fail())
              break;
              GCZ1=line.substr(1,2);
              GCZ2=atof(GCZ1.c_str());
              O_year.push_back(GCZ2);
              GCZ1=line.substr(4,2);
              GCZ2=atof(GCZ1.c_str());
              O_month.push_back(GCZ2);
              GCZ1=line.substr(7,2);
              GCZ2=atof(GCZ1.c_str());
              O_day.push_back(GCZ2);
              GCZ1=line.substr(10,2);
              GCZ2=atof(GCZ1.c_str());
              O_hour.push_back(GCZ2);
              GCZ1=line.substr(13,2);
              GCZ2=atof(GCZ1.c_str());
              O_minute.push_back(GCZ2);
              GCZ1=line.substr(16,10);
              GCZ2=atof(GCZ1.c_str());
              O_second.push_back(GCZ2);
              GCZ1=line.substr(28,1);
              GCZ2=atof(GCZ1.c_str());
              O_LYBZ.push_back(GCZ2);
              GCZ1=line.substr(30,2);
              GCZ4=atoi(GCZ1.c_str());
              SL=GCZ4;
              sum1=0;
              sum2=0;
              sum=0;
              if(SL<=12){
                for(int i=0;i<SL;i++){
                GCZ3=line.substr(32+3*i,3);
                LX1=GCZ3.substr(0,1);
                if(LX1=="G"){
                   O_GCWXS.push_back(GCZ3);
                sum1+=1;
                }
                }
              }
              else{
                for(int i=0;i<12;i++){
                GCZ3=line.substr(32+3*i,3);
                LX1=GCZ3.substr(0,1);
                if(LX1=="G"){
                    O_GCWXS.push_back(GCZ3);
                sum1+=1;
                }
                }
                getline(infile,line);
                for(int i=0;i<SL-12;i++){
                GCZ5=line.substr(32+3*i,3);
                LX2=GCZ5.substr(0,1);
                if(LX2=="G"){
                   O_GCWXS.push_back(GCZ5);
                sum2+=1;
                }
                }
              }
              sum=sum1+sum2;
              O_GPSGS.push_back(sum);//鍗槦涓暟
           if(GCZSL<=5){
              for(int m=0;m<sum;m++){
                  getline(infile,line);
                      for(int n=0;n<GCZSL;n++){
                          GCZ1=line.substr(n*16,16);
                          GCZ2=atof(GCZ1.c_str());
                          O_GCZ[n].push_back(GCZ2);
                      }
              }
              for(int n=0;n<SL-sum;n++)
                  getline(infile,line);

              }
              else{
                   for(int m=0;m<sum;m++){
                       getline(infile,line);
                           for(int n=0;n<5;n++){
                               GCZ1=line.substr(n*16,16);
                               GCZ2=atof(GCZ1.c_str());
                               O_GCZ[n].push_back(GCZ2);
                           }
                         getline(infile,line);
                         for(int n=0;n<GCZSL-5;n++){
                             GCZ1=line.substr(n*16,16);
                             GCZ2=atof(GCZ1.c_str());
                             O_GCZ[n+5].push_back(GCZ2);
                          }
                   }
                   for(int n=0;n<2*(SL-sum);n++)
                       getline(infile,line);
               }
       }
       string prn1;
       double prn;
       int sss=0;
       O_PRN.resize(O_year.size());
       for(int ll=0;ll<O_year.size();ll++){
           if(ll>0)
           sss+=O_GPSGS[ll-1];
       for(int kk=0;kk<O_GPSGS[ll];kk++){
           prn1=O_GCWXS[kk+sss].substr(1,2);
           prn=atof(prn1.c_str());
           O_PRN[ll].push_back(prn);
           }
       }
       infile.close();
}

//读取O文件


void GPS::spp()
{

    O_C1.resize(O_year.size());
    O_P2.resize(O_year.size());
    O_L1.resize(O_year.size());
    O_L2.resize(O_year.size());

    double sum0=0;
    for(int ll0=0;ll0<O_year.size();ll0++){
        if(ll0>0)
        sum0+=O_GPSGS[ll0-1];
    for(int kk0=0;kk0< O_GPSGS[ll0];kk0++){
        O_C1[ll0].push_back(O_GCZ[C1][kk0+sum0]);
        O_P2[ll0].push_back(O_GCZ[P2][kk0+sum0]);
        O_L1[ll0].push_back(O_GCZ[L1][kk0+sum0]);
        O_L2[ll0].push_back(O_GCZ[L2][kk0+sum0]);
        }
    }

int bjz=1;
 for(int j=0;j<O_LYBZ.size();j++)
{
   if(O_LYBZ[j]!=0) continue;
    for(int k=0;k<O_GPSGS[j];k++){
     for(int i=0;i<N_PRN.size();i++){

       if(O_PRN[j][k]==N_PRN[i]){
        double TOC;
        TOC=glg_gps(N_year[i]+2000,N_month[i],N_day[i],N_hour[i],N_minute[i],N_second[i]);
        double TOW;
        TOW=glg_gps(O_year[j]+2000,O_month[j],O_day[j],O_hour[j],O_minute[j],O_second[j]);

        if(fabs(TOW-TOC)<3600){

        bjz+=1;
        double n0,n;
        n0=sqrt(GM/pow(N_sqrta[i],6));
        n=n0+N_n[i];//骞冲潎瑙掗€熷害
        double dltT;
        dltT=TOW-N_wspc[i]-N_TOE[i]-O_C1[j][k]/(3.0e8);//褰掑寲鏃堕棿
        double M1;
        M1=N_M0[i]+n*dltT;//骞宠繎鐐硅
        double E1;
        double E;
        E=M1;
        do{
          E1=E;
          E=M1+N_e[i]*sin(E);
        }while(fabs(E-E1)>1e-15);//鍋忚繎鐐硅
        double r0;
        r0=pow(N_sqrta[i],2)*(1-N_e[i]*cos(E));//鍦板績鐭㈠緞
        double f;
        f=atan(sqrt((1+N_e[i])/(1-N_e[i]))*tan(E/2))*2;//鐪熻繎鐐硅
        double sjd0;
        sjd0=N_w[i]+f;//鍗囦氦鐐硅璺?
        double sdgzu;
        double sdgzr;
        double sdgzi;
        sdgzu=N_cus[i]*sin(2*sjd0)+N_cuc[i]*cos(2*sjd0);
        sdgzr=N_crs[i]*sin(2*sjd0)+N_crc[i]*cos(2*sjd0);
        sdgzi=N_cis[i]*sin(2*sjd0)+N_cic[i]*cos(2*sjd0);//鎽勫姩鏀规椤?
        double sjd,r,i1;
        sjd=sjd0+sdgzu;//鎽勫姩鏀规鐨勫崌浜ょ偣瑙掕窛
        r=r0+sdgzr;//鎽勫姩鏀规鐨勫湴蹇冪煝寰?
        i1=N_i0[i]+N_IDOT[i]*dltT+sdgzi;//鎽勫姩鏀规鐨勮建閬撻潰鍊捐
        double fai;
        fai=N_OMEGA[i]+(N_OMEGADOT[i]-wie)*dltT-wie*N_TOE[i];//瑙傛祴鍘嗗厓鐨勫崌浜ょ偣鐨勭粡搴?
        double x0,y0,z0;
        x0=r*cos(sjd);
        y0=r*sin(sjd);
        z0=r*0;//鍗槦鍦ㄨ建閬撶洿瑙掑潗鏍囩郴涓殑鍧愭爣
        double xt,yt,zt;
        xt=cos(fai)*x0+(-sin(fai)*cos(i1))*y0+sin(fai)*sin(i1)*z0;
        yt=sin(fai)*x0+cos(fai)*cos(i1)*y0+(-cos(fai)*sin(i1))*z0;
        zt=0*x0+sin(i1)*y0+cos(i1)*z0;//鍗槦鍦ㄥ崗璁湴鐞冨潗鏍囩郴鐨勭洿瑙掑潗鏍?
        GPS_xt.push_back(xt);
        GPS_yt.push_back(yt);
        GPS_zt.push_back(zt);
        double tt;
        tt=(N_wspc[i]+N_wspy[i]*(TOW-TOC)+N_wspysd[i]*pow((TOW-TOC),2))*c;
        sat_clock.push_back(tt);//閽熷樊鏀规
        zbzh zxzz;
        zxzz.kjzz_ddzz(APPROX_X,APPROX_Y,APPROX_Z);
        double xx,yy,zz;
        xx=-sin(zxzz.B)*cos(zxzz.L)*(xt-APPROX_X)-sin(zxzz.B)*sin(zxzz.L)*(yt-APPROX_Y)+cos(zxzz.B)*(zt-APPROX_Z);
        yy=-sin(zxzz.L)*(xt-APPROX_X)+cos(zxzz.L)*(yt-APPROX_Y);
        zz=cos(zxzz.B)*cos(zxzz.L)*(xt-APPROX_X)+cos(zxzz.B)*sin(zxzz.L)*(yt-APPROX_Y)+sin(zxzz.B)*(zt-APPROX_Z);
        double El,Az;
        Az=atan2(yy,xx);//鏂逛綅瑙?
        El=atan2(zz,sqrt(pow(xx,2)+pow(yy,2)));//楂樺害瑙?
        Elevation.push_back(El);
        Azimuth.push_back(Az);
        double zz1,zz2;
        zz1=PI/2-El;
        zz2=Trop_delay(zxzz.H,zz1);//瀵规祦灞傛敼姝?
        Trop_Delay.push_back(zz2);
        double pj=sqrt(pow(xt,2)+pow(yt,2)+pow(zt,2));
        double pi=sqrt(pow(APPROX_X,2)+pow(APPROX_Y,2)+pow(APPROX_Z,2));
        double pij=sqrt(pow((APPROX_X-xt),2)+pow((APPROX_Y-yt),2)+pow((APPROX_Z-zt),2));
        double Relativity1=2*u_e*log((pj+pi+pij)/(pj+pi-pij))/(c*c);
        double Relativity2=-2*sqrt(a_s*u_e)*N_e[i]*sin(E)/c;
        Relativity.push_back(Relativity1+Relativity2);//鐩稿璁烘敼姝?
        double sagnac=-wie*((xt-APPROX_X)*yt-(yt-APPROX_Y)*xt)/c;//鍦扮悆鑷浆鏀规
        Sagnac.push_back(sagnac);
        break;
       }
     }
     }
     if(bjz==1){
         GPS_xt.push_back(0);
         GPS_yt.push_back(0);
         GPS_zt.push_back(0);
         sat_clock.push_back(0);
         Elevation.push_back(0);
         Azimuth.push_back(0);
         Trop_Delay.push_back(0);
         Relativity.push_back(0);
         Sagnac.push_back(0);
         }
     bjz=1;
    }
 }
    GPS_Xt.resize(O_year.size());
    GPS_Yt.resize(O_year.size());
    GPS_Zt.resize(O_year.size());
    GPS_sat_clock.resize(O_year.size());
     GPS_Elevation.resize(O_year.size());
    GPS_Azimuth.resize(O_year.size());
    GPS_Trop_Delay.resize(O_year.size());
    GPS_Relativity.resize(O_year.size());
    GPS_Sagnac.resize(O_year.size());

    int sum4=0;
    for(int ll=0;ll<O_year.size();ll++){
        if(ll>0)
        sum4+=O_GPSGS[ll-1];
    for(int kk=0;kk<O_GPSGS[ll];kk++){
        GPS_Xt[ll].push_back(GPS_xt[kk+sum4]);
        GPS_Yt[ll].push_back(GPS_yt[kk+sum4]);
        GPS_Zt[ll].push_back(GPS_zt[kk+sum4]);
        GPS_sat_clock[ll].push_back(sat_clock[kk+sum4]);
        GPS_Elevation[ll].push_back(Elevation[kk+sum4]);
        GPS_Azimuth[ll].push_back(Azimuth[kk+sum4]);
        GPS_Trop_Delay[ll].push_back(Trop_Delay[kk+sum4]);
        GPS_Relativity[ll].push_back(Relativity[kk+sum4]);
        GPS_Sagnac[ll].push_back(Sagnac[kk+sum4]);

        }


    }

}



void SCPC::pingcha(GPS mob,GPS ref){
    double f1=154*f0;
    double f2=120*f0;
    double l1,m1,n1,LL1;
    double Pip1,Rip1;
    double l2,m2,n2,LL2;
    double Pip2,Rip2;
    double TOW1,TOW2;

    vector<double> Xt1;
    vector<double> Yt1;
    vector<double> Zt1;
    vector<double> sat_clock1;
    vector<double> Elevation1;
    vector<double> Azimuth1;
    vector<double> Trop_Delay1;
    vector<double> Relativity1;
    vector<double> Sagnac1;
    vector<double> C11;
    vector<double> P21;
    vector<double> L11;
    vector<double> L21;
    vector<double> Xt2;
    vector<double> Yt2;
    vector<double> Zt2;
    vector<double> sat_clock2;
    vector<double> Elevation2;
    vector<double> Azimuth2;
    vector<double> Trop_Delay2;
    vector<double> Relativity2;
    vector<double> Sagnac2;
    vector<double> C12;
    vector<double> P22;
    vector<double> L12;
    vector<double> L22;
    vector<double> PRN1;
    vector<double> PRN2;

    vector<double>a1;
    vector<vector<double> > A1;
    vector<double>L1;
    vector<double>a2;
    vector<vector<double> > A;
    vector<double>L;

    vector<int> number;
    for(int i=0;i<mob.O_GPSGS.size();i++){
        for(int a=0;a<mob.O_GPSGS[i];a++){
            for(int k=0;k<ref.O_GPSGS.size();k++){
                TOW1=glg_gps(mob.O_year[i]+2000,mob.O_month[i],mob.O_day[i],mob.O_hour[i],mob.O_minute[i],mob.O_second[i]);
                TOW2=glg_gps(ref.O_year[k]+2000,ref.O_month[k],ref.O_day[k],ref.O_hour[k],ref.O_minute[k],ref.O_second[k]);
                if(TOW1==TOW2){

                    for(int b=0;b<ref.O_GPSGS[k];b++){
                        if(mob.O_PRN[i][a]==ref.O_PRN[k][b]){
                            if((mob.GPS_Xt[i][a]!=0)&&(mob.O_C1[i][a]!=0)&&(mob.O_P2[i][a]!=0)&&(ref.GPS_Xt[k][b]!=0)&&(ref.O_C1[k][b]!=0)&&(ref.O_P2[k][b]!=0)){

                                //cout<<i<<a<<","<<k<<b<<endl;
                                Xt1.push_back(mob.GPS_Xt[i][a]);
                                Xt2.push_back(ref.GPS_Xt[k][b]);

                                Yt1.push_back(mob.GPS_Yt[i][a]);
                                Yt2.push_back(ref.GPS_Yt[k][b]);

                                Zt1.push_back(mob.GPS_Zt[i][a]);
                                Zt2.push_back(ref.GPS_Zt[k][b]);

                                sat_clock1.push_back(mob.GPS_sat_clock[i][a]);
                                sat_clock2.push_back(ref.GPS_sat_clock[k][b]);

                                Elevation1.push_back(mob.GPS_Elevation[i][a]);
                                Elevation2.push_back(ref.GPS_Elevation[k][b]);

                                Azimuth1.push_back(mob.GPS_Azimuth[i][a]);
                                Azimuth2.push_back(ref.GPS_Azimuth[k][b]);

                                Trop_Delay1.push_back(mob.GPS_Trop_Delay[i][a]);
                                Trop_Delay2.push_back(ref.GPS_Trop_Delay[k][b]);

                                Relativity1.push_back(mob.GPS_Relativity[i][a]);
                                Relativity2.push_back(ref.GPS_Relativity[k][b]);

                                Sagnac1.push_back(mob.GPS_Sagnac[i][a]);
                                Sagnac2.push_back(ref.GPS_Sagnac[k][b]);

                                C11.push_back(mob.O_C1[i][a]);
                                C12.push_back(ref.O_C1[k][b]);

                                P21.push_back(mob.O_P2[i][a]);
                                P22.push_back(ref.O_P2[k][b]);

                                L11.push_back(mob.O_L1[i][a]);
                                L12.push_back(ref.O_L1[k][b]);

                                L21.push_back(mob.O_L2[i][a]);
                                L22.push_back(ref.O_L2[k][b]);

                                PRN1.push_back(mob.O_PRN[i][a]);
                                PRN2.push_back(ref.O_PRN[k][b]);
                            }

                        }
                    }
                    break;
                }
            }
        }
        spp_Xt1.push_back(Xt1);
        spp_Xt2.push_back(Xt2);
        Xt1.clear();
        Xt2.clear();

        spp_Yt1.push_back(Yt1);
        spp_Yt2.push_back(Yt2);
        Yt1.clear();
        Yt2.clear();

        spp_Zt1.push_back(Zt1);
        spp_Zt2.push_back(Zt2);
        Zt1.clear();
        Zt2.clear();

        spp_sat_clock1.push_back(sat_clock1);
        spp_sat_clock2.push_back(sat_clock2);
        sat_clock1.clear();
        sat_clock2.clear();

        spp_Elevation1.push_back(Elevation1);
        spp_Elevation2.push_back(Elevation2);
        Elevation1.clear();
        Elevation2.clear();

        spp_Azimuth1.push_back(Azimuth1);
        spp_Azimuth2.push_back(Azimuth2);
        Azimuth1.clear();
        Azimuth2.clear();

        spp_Trop_Delay1.push_back(Trop_Delay1);
        spp_Trop_Delay2.push_back(Trop_Delay2);
        Trop_Delay1.clear();
        Trop_Delay2.clear();

        spp_Relativity1.push_back(Relativity1);
        spp_Relativity2.push_back(Relativity2);
        Relativity1.clear();
        Relativity2.clear();

        spp_Sagnac1.push_back(Sagnac1);
        spp_Sagnac2.push_back(Sagnac2);
        Sagnac1.clear();
        Sagnac2.clear();

        spp_C11.push_back(C11);
        spp_C12.push_back(C12);
        C11.clear();
        C12.clear();

        spp_P21.push_back(P21);
        spp_P22.push_back(P22);
        P21.clear();
        P22.clear();

        spp_L11.push_back(L11);
        spp_L12.push_back(L12);
        L11.clear();
        L12.clear();

        spp_L21.push_back(L21);
        spp_L22.push_back(L22);
        L21.clear();
        L22.clear();

        spp_PRN1.push_back(PRN1);
        spp_PRN2.push_back(PRN2);
        PRN1.clear();
        PRN2.clear();
    }
    mob.SZclear();
    ref.SZclear();
    for(int f=0;f<spp_Elevation1.size();f++){
        for(int g=0;g<spp_Elevation1[f].size();g++){
            if(spp_Elevation1[f][g]<0)
                spp_Elevation1[f][g]=fabs(spp_Elevation1[f][g]/PI*180+180-90);
            else
                spp_Elevation1[f][g]=fabs(spp_Elevation1[f][g]/PI*180);
        }

    }
    vector<double>maxEl;
    double max=0;
    for(int m=0;m<spp_Elevation1.size();m++){
        max=spp_Elevation1[m][0];
        for(int n=0;n<spp_Elevation1[m].size();n++){
            if(spp_Elevation1[m][n]<=max)
                max=spp_Elevation1[m][n];
        }
        maxEl.push_back(max);
    }

    for(int m1=0;m1<spp_Elevation1.size();m1++){
        for(int n1=0;n1<spp_Elevation1[m1].size();n1++){
            if(spp_Elevation1[m1][n1]==maxEl[m1]){
                //cout<<spp_Elevation1[m1][n1]<<"  "<<maxEl[m1]<<endl;
                number.push_back(n1);
            }
        }
    }

    maxEl.clear();
    for(int i=0;i<spp_Xt1.size();i++){
        for(int j=0;j<spp_Xt1[i].size();j++){
            //if((spp_Xt1[i][j]!=0)&&(spp_C11[i][j]!=0)&&(spp_P21[i][j]!=0)&&(spp_Xt2[i][j]!=0)&&(spp_C12[i][j]!=0)&&(spp_P22[i][j]!=0)){
            Pip1=(f1*f1*spp_C11[i][j]- f2*f2*spp_P21[i][j])/(f1*f1-f2*f2);
            Rip1=sqrt((spp_Xt1[i][j]-mob.APPROX_X)*(spp_Xt1[i][j]-mob.APPROX_X)+(spp_Yt1[i][j]-mob.APPROX_Y)*(spp_Yt1[i][j]-mob.APPROX_Y)
                      +(spp_Zt1[i][j]-mob.APPROX_Z)*(spp_Zt1[i][j]-mob.APPROX_Z));
            l1=(mob.APPROX_X-spp_Xt1[i][j])/Rip1;
            m1=(mob.APPROX_Y-spp_Yt1[i][j])/Rip1;
            n1=(mob.APPROX_Z-spp_Zt1[i][j])/Rip1;
            LL1=Pip1-Rip1+spp_sat_clock1[i][j]-spp_Trop_Delay1[i][j]+spp_Relativity1[i][j]+spp_Sagnac1[i][j];

            Pip2=(f1*f1*spp_C12[i][j]- f2*f2*spp_P22[i][j])/(f1*f1-f2*f2);
            Rip2=sqrt((spp_Xt2[i][j]-ref.APPROX_X)*(spp_Xt2[i][j]-ref.APPROX_X)+(spp_Yt2[i][j]-ref.APPROX_Y)*(spp_Yt2[i][j]-ref.APPROX_Y)
                      +(spp_Zt2[i][j]-ref.APPROX_Z)*(spp_Zt2[i][j]-ref.APPROX_Z));
            l2=(ref.APPROX_X-spp_Xt2[i][j])/Rip2;
            m2=(ref.APPROX_Y-spp_Yt2[i][j])/Rip2;
            n2=(ref.APPROX_Z-spp_Zt2[i][j])/Rip2;
            LL2=Pip2-Rip2+spp_sat_clock2[i][j]-spp_Trop_Delay2[i][j]+spp_Relativity2[i][j]+spp_Sagnac2[i][j];

            a1.push_back(l1-l2);
            a1.push_back(m1-m2);
            a1.push_back(n1-n2);
            A1.push_back(a1);
            a1.clear();

            L1.push_back(LL1-LL2);
            // }
            // else if(j<=number[i]){
            ////         number[i]-=1;
            // }
        }
        double czz1=0,lll1=0;
        for(int ww=0;ww<A1.size();ww++){
            if(ww!=number[i]){
                for(int zz=0;zz<3;zz++){
                    czz1=A1[ww][zz]-A1[number[i]][zz];
                    a2.push_back(czz1);
                }
                lll1=L1[ww]-L1[number[i]];
                L.push_back(lll1);
                A.push_back(a2);
                a2.clear();

            }

        }


        if(L.size()<4)
            continue;
        double N[3][3];
        for(int ii=0;ii<3;ii++){
            for(int jj=0;jj<3;jj++){
                N[ii][jj]=0;
                for(int kk=0;kk<L.size();kk++){
                    N[ii][jj]+=A[kk][ii]*A[kk][jj];
                }
            }
        }
        DinV(N,3);
        double W[3];
        double xx[3];
        for(int ii=0;ii<3;ii++){
            W[ii]=0;
            for(int kk=0;kk<L.size();kk++){
                W[ii]+=A[kk][ii]*L[kk];
            }
        }

        for(int ii=0;ii<3;ii++){
            xx[ii]=0;
            for(int kk=0;kk<3;kk++){
                xx[ii]+=N[ii][kk]*W[kk];
            }
        }
        double Xp = ref.APPROX_X + xx[0];
        double Yp = ref.APPROX_Y + xx[1];
        double Zp = ref.APPROX_Z + xx[2];

        Pcx.push_back(xx[0]);
        Pcy.push_back(xx[1]);
        Pcz.push_back(xx[2]);

        double xxx,yyy,zzz;
        zbzh zxzz;

        zxzz.kjzz_ddzz(ref.APPROX_X,ref.APPROX_Y,ref.APPROX_Z);
        xxx=-sin(zxzz.B)*cos(zxzz.L)*(Xp-ref.APPROX_X)-sin(zxzz.B)*sin(zxzz.L)*(Yp-ref.APPROX_Y)+cos(zxzz.B)*(Zp-ref.APPROX_Z);
        yyy=-sin(zxzz.L)*(Xp-ref.APPROX_X)+cos(zxzz.L)*(Yp-ref.APPROX_Y);
        zzz=cos(zxzz.B)*cos(zxzz.L)*(Xp-ref.APPROX_X)+cos(zxzz.B)*sin(zxzz.L)*(Yp-ref.APPROX_Y)+sin(zxzz.B)*(Zp-ref.APPROX_Z);

        N1.push_back(xxx);
        E1.push_back(yyy);
        U1.push_back(zzz);

        L1.clear();
        A1.clear();
        A.clear();
        L.clear();
    }

    ofstream outfile1("C:\\Users\\Administrator\\Desktop\\GPS_SC\\error value.txt");
    for(int j=0;j<Pcx.size();++j)
    {
        outfile1<<setw(10)<<fixed<<setprecision(4)<<Pcx[j]<<",";
        outfile1<<setw(10)<<fixed<<setprecision(4)<<Pcy[j]<<",";
        outfile1<<setw(10)<<fixed<<setprecision(4)<<Pcz[j]<<endl;

    }

    ofstream outfile2("E:\\N\\SHUCHU.txt");
    for(int j=0;j<Pcx.size();++j)
    {
        outfile2<<setw(10)<<fixed<<setprecision(4)<<N1[j]<<",";
        outfile2<<setw(10)<<fixed<<setprecision(4)<<E1[j]<<",";
        outfile2<<setw(10)<<fixed<<setprecision(4)<<U1[j]<<endl;

    }
    Pcx.clear();
    Pcy.clear();
    Pcz.clear();
    N1.clear();
    E1.clear();
    U1.clear();
    spp_Xt1.clear();
    spp_Yt1.clear();
    spp_Zt1.clear();
    spp_sat_clock1.clear();
    spp_Elevation1.clear();
    spp_Azimuth1.clear();
    spp_Trop_Delay1.clear();
    spp_Relativity1.clear();
    spp_Sagnac1.clear();
    spp_C11.clear();
    spp_P21.clear();
    spp_L11.clear();
    spp_L21.clear();
    spp_Xt2.clear();
    spp_Yt2.clear();
    spp_Zt2.clear();
    spp_sat_clock2.clear();
    spp_Elevation2.clear();
    spp_Azimuth2.clear();
    spp_Trop_Delay2.clear();
    spp_Relativity2.clear();
    spp_Sagnac2.clear();
    spp_C12.clear();
    spp_P22.clear();
    spp_L12.clear();
    spp_L22.clear();
    spp_PRN1.clear();
    spp_PRN2.clear();



}







































double glg_gps(double Y,double M,double D,double UT1,double UT2,double UT3)
{
    double y,m,JD;
    double TOW,UT;
    UT=UT1+UT2/60+UT3/3600;
    if(M<=2){
        y=Y-1;
        m=M+12;
        JD=floor(365.25*y)+floor(30.6001*(m+1))+D+UT/24+1720981.5;
    }
    else
    {
        y=Y;
        m=M;
        JD=floor(365.25*y)+floor(30.6001*(m+1))+D+UT/24+1720981.5;
    }
    TOW=fmod(JD-2444244.5,7)*86400;//格里高利转为GPS时
    return TOW;
}

void zbzh::kjzz_ddzz(double X, double Y, double Z)
{
    double B1;
    double N,cz;
    L=atan2(Y,X);
    B=atan2(Z,sqrt(X*X+Y*Y));
    do{
        N=aa/sqrt(1-e2*sin(B)*sin(B));
        H=Z/sin(B)-N*(1-e2);
        B1=B;
        B=atan2(Z*(N+H),sqrt(X*X+Y*Y)*(N*(1-e2)+H));
        cz=B-B1;
    }while(cz>1e-12);
}

double Trop_delay(double H,double z)
{
    double Tropdelay;
    double H0=0,P0=1013.25,T0=18,Rh0=0.5,RE=6378137;
    double hd,hw =11000,rd,rw,ad,aw,bd,bw;
    double TropDelay_w,TropDelay_d,P,T,Rh,e,Nw,Nd;
    P=P0*pow((1-0.000226*(H-H0)),5.225);
    T=T0-0.0065*(H-H0)+273.16;
    Rh=Rh0*exp(-0.0006396*(H-H0));
    e=Rh*exp(-37.2465+0.213166*T-0.000256908*T*T);
    Nd=77.64*P/T;
    Nw=-12.96*e/T+371800*e/(T*T);
    hd=40136+148.72*(T-273.16);
    rd=sqrt((RE+hd)*(RE+hd)-RE*RE*sin(z)*sin(z))-RE*cos(z);
    rw=sqrt((RE+hw)*(RE+hw)-RE*RE*sin(z)*sin(z))-RE*cos(z);
    ad=-cos(z)/hd;
    aw=-cos(z)/hw;
    bd=-sin(z)*sin(z)/(2*hd*RE);
    bw=-sin(z)*sin(z)/(2*hw*RE);
    TropDelay_d=1e-6*Nd*(rd+4*ad*rd*rd/2+(6*ad*ad+4*bd)*rd*rd*rd/3+4*ad*(ad*ad+3*bd)*pow(rd,4)/4
                         +(pow(ad,4)+12*ad*ad*bd+6*bd*bd)*pow(rd,5)/5+4*ad*bd*(ad*ad+3*bd)*pow(rd,6)/6
                         +bd*bd*(6*ad*ad+4*bd)*pow(rd,7)/7+4*ad*bd*bd*bd*pow(rd,8)/8+pow(bd,4)*pow(rd,9)/9);
    TropDelay_w=1e-6*Nw*(rw+4*aw*rw*rw/2+(6*aw*aw+4*bw)*rw*rw*rw/3+4*aw*(aw*aw+3*bw)*pow(rw,4)/4
                         +(pow(aw,4)+12*aw*aw*bw+6*bw*bw)*pow(rw,5)/5+4*aw*bw*(aw*aw+3*bw)*pow(rw,6)/6
                         +bw*bw*(6*aw*aw+4*bw)*pow(rw,7)/7+4*aw*bw*bw*bw*pow(rw,8)/8+pow(bw,4)*pow(rw,9)/9);
    Tropdelay=TropDelay_d+TropDelay_w;
    return Tropdelay;
}

int DinV(double A[][NN],int n)
{
    int i,j,k;
    double d;
    int JS[NN],IS[NN];
    for(k=0;k<n;k++){
        d=0;
        for(i=k;i<n;i++)
            for(j=k;j<n;j++)
            {
                if (fabs(A[i][j])>d)
                {
                    d=fabs(A[i][j]);
                    IS[k]=i;
                    JS[k]=j;
                }
            }
        if(d+1.0==1.0) return 0;

        if(IS[k]!=k)
            for(j=0;j<n;j++)
                swap(A[k][j],A[IS[k]][j]);
        if(JS[k]!=k)
            for(i=0;i<n;i++)
                swap(A[i][k],A[i][JS[k]]);
        A[k][k]=1/A[k][k];
        for(j=0;j<n;j++)
            if(j!=k)A[k][j]=A[k][j]*A[k][k];
        for(i=0;i<n;i++)
            if(i!=k)
                for(j=0;j<n;j++)
                    if(j!=k) A[i][j]=A[i][j]-A[i][k]*A[k][j];
        for(i=0;i<n;i++)
            if(i!=k)A[i][k]=-A[i][k]*A[k][k];
    }
    for(k=n-1;k>=0;k--)
    {
        for(j=0;j<n;j++)
            if(JS[k]!=k)swap(A[k][j],A[JS[k]][j]);
        for(i=0;i<n;i++)
            if(IS[k]!=k)swap(A[i][k],A[i][IS[k]]);
    }
}
void GPS::SZclear(){
    N_PRN.clear();
    N_year.clear();
    N_month.clear();
    N_day.clear();
    N_hour.clear();
    N_minute.clear();
    N_second.clear();
    N_wspc.clear();
    N_wspy.clear();
    N_wspysd.clear();
    N_IODE.clear();
    N_crs.clear();
    N_n.clear();
    N_M0.clear();
    N_cuc.clear();
    N_e.clear();
    N_cus.clear();
    N_sqrta.clear();
    N_TOE.clear();
    N_cic.clear();
    N_OMEGA.clear();
    N_cis.clear();
    N_i0.clear();
    N_crc.clear();
    N_w.clear();
    N_OMEGADOT.clear();
    N_IDOT.clear();
    N_L2.clear();
    N_zs.clear();
    N_L2P.clear();
    N_m.clear();
    N_wszt.clear();
    N_TGD.clear();
    N_IODC.clear();
    N_wsfs.clear();
    N_nhqj.clear();
    O_GCZLX.clear();
    O_year.clear();
    O_month.clear();
    O_day.clear();
    O_hour.clear();
    O_minute.clear();
    O_second.clear();
    O_LYBZ.clear();
    O_WSGS.clear();
    O_GCWXS.clear();
    O_SUM.clear();
    O_GPSGS.clear();
    O_GCZ.clear();
    O_PRN.clear();
    O_GuanCeZhi.clear();

    GPS_xt.clear();
    GPS_yt.clear();
    GPS_zt.clear();
    sat_clock.clear();
    Elevation.clear();
    Azimuth.clear();
    Relativity.clear();
    Trop_Delay.clear();
    Sagnac.clear();
    GPS_Xt.clear();
    GPS_Yt.clear();
    GPS_Zt.clear();
    GPS_sat_clock.clear();
    GPS_Elevation.clear();
    GPS_Azimuth.clear();
    GPS_Trop_Delay.clear();
    GPS_Relativity.clear();
    GPS_Sagnac.clear();
    O_C1.clear();
    O_P2.clear();
    O_L1.clear();
    O_L2.clear();
}


