#ifndef STATIC
#define STATIC
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#define GM 3.986005e+14
#define wie 7.2921151466999996e-05
#define e2 0.00669437999014132
#define aa 6378137.0
#define u_e 3.986004418e+14
#define c 299792458.0
#define a_s 26560000
#define PI 3.141592653589
#define f0 10.23
#define NN 3
using namespace std;
class gps
{
public:
    vector<double>N_PRN;
    vector<double>N_year;
    vector<double>N_month;
    vector<double>N_day;
    vector<double>N_hour;
    vector<double>N_minute;
    vector<double>N_second;
    vector<double>N_wspc;
    vector<double>N_wspy;
    vector<double>N_wspysd;
    vector<double>N_IODE;
    vector<double>N_crs;
    vector<double>N_n;
    vector<double>N_M0;
    vector<double>N_cuc;
    vector<double>N_e;
    vector<double>N_cus;
    vector<double>N_sqrta;
    vector<double>N_TOE;
    vector<double>N_cic;
    vector<double>N_OMEGA;
    vector<double>N_cis;
    vector<double>N_i0;
    vector<double>N_crc;
    vector<double>N_w;
    vector<double>N_OMEGADOT;
    vector<double>N_IDOT;
    vector<double>N_L2;
    vector<double>N_zs;
    vector<double>N_L2P;
    vector<double>N_m;
    vector<double>N_wszt;
    vector<double>N_TGD;
    vector<double>N_IODC;
    vector<double>N_wsfs;
    vector<double>N_nhqj;
    //N文件的数据
    double APPROX_X;
    double APPROX_Y;
    double APPROX_Z;
    double GCZSL;
    vector<string>O_GCZLX;
    vector<double>O_year;
    vector<double>O_month;
    vector<double>O_day;
    vector<double>O_hour;
    vector<double>O_minute;
    vector<double>O_second;
    vector<double>O_LYBZ;
    vector<double>O_WSGS;
    vector<string>O_GCWXS;
    vector<vector<double> >O_GCZ;
    vector<vector<double> >O_PRN;
    vector<vector<double> >O_C1;
    vector<vector<double> >O_P2;
    vector<vector<double> >O_L1;
    vector<vector<double> >O_L2;
    //O文件的数据
    vector<double>GPS_xt;
    vector<double>GPS_yt;
    vector<double>GPS_zt;
    vector<double>sat_clock;
    vector<double>Azimuth;
    vector<double>Elevation;
    vector<double>Trop_Delay;
    vector<double>Relativity;
    vector<double>Sagnac;
    vector<vector<double> > GPS_Xt;
    vector<vector<double> > GPS_Yt;
    vector<vector<double> > GPS_Zt;
    vector<vector<double> > GPS_sat_clock;
    vector<vector<double> > GPS_Elevation;
    vector<vector<double> > GPS_Azimuth;
    vector<vector<double> > GPS_Trop_Delay;
    vector<vector<double> > GPS_Relativity;
    vector<vector<double> > GPS_Sagnac;
    vector<vector<double> > GPS_NF;
    vector<vector<double> > GPS_MWGCZ;
    void Nin();
    void Oin(string filename);
    void Nout();
    void Oout();
    //读取文件
    void SPP();
    //生成spp文件
    void Singledifference();
    //单差部分
    void spp_out(string filename);
    double glg_gps(double Y,double M,double D,double UT1,double UT2,double UT3);
    void kjzz_ddzz(double X,double Y,double Z);
    double B;double L;double H;
    double Trop_delay(double H,double z);
};
class SCPC
{
public:
    vector<vector<double> > spp_Xt1;
    vector<vector<double> > spp_Yt1;
    vector<vector<double> > spp_Zt1;
    vector<vector<double> > spp_sat_clock1;
    vector<vector<double> > spp_Elevation1;
    vector<vector<double> > spp_Azimuth1;
    vector<vector<double> > spp_Trop_Delay1;
    vector<vector<double> > spp_Relativity1;
    vector<vector<double> > spp_Sagnac1;
    vector<vector<double> > spp_C11;
    vector<vector<double> > spp_P21;
    vector<vector<double> > spp_L11;
    vector<vector<double> > spp_L21;
    vector<vector<double> > spp_Xt2;
    vector<vector<double> > spp_Yt2;
    vector<vector<double> > spp_Zt2;
    vector<vector<double> > spp_sat_clock2;
    vector<vector<double> > spp_Elevation2;
    vector<vector<double> > spp_Azimuth2;
    vector<vector<double> > spp_Trop_Delay2;
    vector<vector<double> > spp_Relativity2;
    vector<vector<double> > spp_Sagnac2;
    vector<vector<double> > spp_C12;
    vector<vector<double> > spp_P22;
    vector<vector<double> > spp_L12;
    vector<vector<double> > spp_L22;
    vector<vector<double> > spp_PRN1;
    vector<vector<double> > spp_PRN2;
    vector<double> Pcx;
    vector<double> Pcy;
    vector<double> Pcz;
    vector<double> N1;
    vector<double> E1;
    vector<double> U1;
    void Adjustment(gps mob, gps ref);
};
//双差部分
#endif // STATIC

