#include "static.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

void gps::Nin()
{
    ifstream open("E:\\zbsj\\master.11N",ios::in);
    if(!open)
    {
        cerr<<"not open"<<endl;
        exit(EXIT_FAILURE);
    }
    string line,line1;
    double median;
    while(line.find("END OF HEADER")== string::npos)

    {   getline(open,line);

    }
    while(!open.eof())
    {
       getline(open,line);
       if (open.fail())
       break;

       line1=line.substr(0,2);
       median=atof(line1.c_str());
       N_PRN.push_back(median);

       line1=line.substr(3,2);
       median=atof(line1.c_str());
       N_year.push_back(median);

       line1=line.substr(6,2);
       median=atof(line1.c_str());
       N_month.push_back(median);

       line1=line.substr(9,2);
       median=atof(line1.c_str());
       N_day.push_back(median);

       line1=line.substr(12,2);
       median=atof(line1.c_str());
       N_hour.push_back(median);

       line1=line.substr(15,2);
       median=atof(line1.c_str());
       N_minute.push_back(median);


       line1=line.substr(18,4);
       median=atof(line1.c_str());
       N_second.push_back(median);


       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_wspc.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_wspy.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_wspysd.push_back(median);


       getline(open,line);


       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_IODE.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_crs.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_n.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_M0.push_back(median);

       getline(open,line);

       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_cuc.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_e.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_cus.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_sqrta.push_back(median);


       getline(open,line);

       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_TOE.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_cic.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_OMEGA.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_cis.push_back(median);


       getline(open,line);


       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_i0.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_crc.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_w.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_OMEGADOT.push_back(median);

       getline(open,line);

       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_IDOT.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_L2.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_zs.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_L2P.push_back(median);

       getline(open,line);

       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_m.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_wszt.push_back(median);

       line1=line.substr(41,19);
       median=atof(line1.c_str());
       N_TGD.push_back(median);

       line1=line.substr(60,19);
       median=atof(line1.c_str());
       N_IODC.push_back(median);

       getline(open,line);

       line1=line.substr(3,19);
       median=atof(line1.c_str());
       N_wsfs.push_back(median);

       line1=line.substr(22,19);
       median=atof(line1.c_str());
       N_nhqj.push_back(median);
    }
    open.close();
}


void gps::Oin(string filename)
{
    string line,line1;
    double median;
    ifstream open( filename,ios::in);
    if(!open)
    {
        cerr<<"not open"<<endl;
        exit(EXIT_FAILURE);
    }
    string APPROX_X1,APPROX_Y1,APPROX_Z1,num_leixing1,leixing2;

    do
    {
        getline(open, line);
        if(line.find("APPROX POSITION XYZ")!= string::npos)
        {
            APPROX_X1=line.substr(1,13);
            APPROX_Y1=line.substr(15,13);
            APPROX_Z1=line.substr(29,13);
            APPROX_X=atof(APPROX_X1.c_str());
            APPROX_Y=atof(APPROX_Y1.c_str());
            APPROX_Z=atof(APPROX_Z1.c_str());
        }

        else if(line.find("# / TYPES OF OBSERV")!= string::npos)
        {
            num_leixing1=line.substr(5,1);
            GCZSL=atoi(num_leixing1.c_str());
            for(unsigned int j=0;j<GCZSL;j++)
            {
                leixing2=line.substr(10+j*6,2);
                O_GCZLX.push_back(leixing2);
            }
        }
    }
    while(line.find("END OF HEADER") == string::npos);
    while(!open.eof())
    {
        getline(open,line);
        if(open.fail())
            break;
        line1=line.substr(1,2);
        median=atof(line1.c_str());
        O_year.push_back(median);

        line1=line.substr(4,2);
        median=atof(line1.c_str());
        O_month.push_back(median);

        line1=line.substr(7,2);
        median=atof(line1.c_str());
        O_day.push_back(median);

        line1=line.substr(10,2);
        median=atof(line1.c_str());
        O_hour.push_back(median);

        line1=line.substr(13,2);
        median=atof(line1.c_str());
        O_minute.push_back(median);

        line1=line.substr(16,10);
        median=atof(line1.c_str());
        O_second.push_back(median);

        line1=line.substr(28,1);
        median=atof(line1.c_str());
        O_LYBZ.push_back(median);

        line1=line.substr(30,2);
        median=atoi(line1.c_str());
        O_WSGS.push_back(median);

        for(unsigned int i=0;i<median;i++)
        {
            line1=line.substr(32+3*i,3);
            O_GCWXS.push_back(line1);
        }
        /*for(unsigned int i=0;i<median;i++)
        {
          getline(open,line);
          cout<<line<<endl;
        }*/ //测试下面的代码

       O_GCZ.resize(GCZSL);
       string line2;double median1;




       for(unsigned int j=0;j<median;j++)
       {
           getline(open,line);

           for(unsigned int i=0;i<GCZSL;i++)
           {



              line2=line.substr(16*i,15);

               median1=atof(line2.c_str());

               O_GCZ[i].push_back(median1);


           }
       }




    }


    O_PRN.resize(O_year.size());
    int sum=0;
    string prn;int prn1;
    for(unsigned int i=0;i<O_year.size();i++)
    {
       if(i>0)
       sum+=O_WSGS[i-1];
       for(unsigned int j=0;j<O_WSGS[i];j++)
       {
           prn=O_GCWXS[j+sum].substr(1,2);
           prn1=atoi(prn.c_str());
           O_PRN[i].push_back(prn1);


       }
    }





    O_C1.resize(O_year.size());
    O_P2.resize(O_year.size());
    O_L1.resize(O_year.size());
    O_L2.resize(O_year.size());
    int sum1=0;
    for(unsigned int i=0;i<O_year.size();i++)
    {
        if(i>0)
        sum1+=O_WSGS[i-1];
        for(unsigned int j=0;j<O_WSGS[i];j++)
        {
            O_C1[i].push_back(O_GCZ[0][j+sum1]);
            O_P2[i].push_back(O_GCZ[1][j+sum1]);
            O_L1[i].push_back(O_GCZ[2][j+sum1]);
            O_L2[i].push_back(O_GCZ[3][j+sum1]);
        }
    }



     open.close();

}


void gps::SPP()
{
    int bjz=1;
    for(unsigned int i=0;i<O_year.size();i++)
    {
        if(O_LYBZ[i] !=0) continue;
        for(unsigned int j=0;j<O_WSGS[i];j++)
        {
            for(unsigned int k=0;k<N_PRN.size();k++)
            {

                if(O_PRN[i][j]==N_PRN[k])
                {

                    double TOC;
                    TOC=glg_gps(N_year[k]+2000,N_month[k],N_day[k],N_hour[k],N_minute[k],N_second[k]);
                    double TOW;
                    TOW=glg_gps(O_year[i]+2000,O_month[i],O_day[i],O_hour[i],O_minute[i],O_second[i]);
                    if(fabs(TOW-TOC)<3600)
                    {
                        bjz+=1;
                        double n0,n;
                        n0=sqrt(GM/pow(N_sqrta[k],6));
                        n=n0+N_n[k];//平均角速度
                        double dltT;
                        dltT=TOW-N_wspc[k]-N_TOE[k]-O_C1[i][j]/(3.0e8);//归化时间
                        double M1;
                        M1=N_M0[k]+n*dltT;//平近点角
                        double E1;
                        double E;
                        E=M1;
                        do{
                            E1=E;
                            E=M1+N_e[k]*sin(E);
                        }while(fabs(E-E1)>1e-15);//偏近点角
                        double r0;
                        r0=pow(N_sqrta[k],2)*(1-N_e[k]*cos(E));//地心矢径
                        double f;
                        f=atan(sqrt((1+N_e[k])/(1-N_e[k]))*tan(E/2))*2;//真近点角
                        double sjd0;
                        sjd0=N_w[k]+f;//升交点角距
                        double sdgzu;
                        double sdgzr;
                        double sdgzi;
                        sdgzu=N_cus[k]*sin(2*sjd0)+N_cuc[k]*cos(2*sjd0);
                        sdgzr=N_crs[k]*sin(2*sjd0)+N_crc[k]*cos(2*sjd0);
                        sdgzi=N_cis[k]*sin(2*sjd0)+N_cic[k]*cos(2*sjd0);//摄动改正项
                        double sjd,r,i1;
                        sjd=sjd0+sdgzu;//摄动改正的升交点角距
                        r=r0+sdgzr;//摄动改正的地心矢径
                        i1=N_i0[k]+N_IDOT[k]*dltT+sdgzi;//摄动改正的轨道面倾角
                        double fai;
                        fai=N_OMEGA[k]+(N_OMEGADOT[k]-wie)*dltT-wie*N_TOE[k];//观测历元的升交点的经度
                        double x0,y0,z0;
                        x0=r*cos(sjd);
                        y0=r*sin(sjd);
                        z0=r*0;//卫星在轨道直角坐标系中的坐标
                        double xt,yt,zt;
                        xt=cos(fai)*x0+(-sin(fai)*cos(i1))*y0+sin(fai)*sin(i1)*z0;
                        yt=sin(fai)*x0+cos(fai)*cos(i1)*y0+(-cos(fai)*sin(i1))*z0;
                        zt=0*x0+sin(i1)*y0+cos(i1)*z0;//卫星在协议地球坐标系的直角坐标
                        GPS_xt.push_back(xt);
                        GPS_yt.push_back(yt);
                        GPS_zt.push_back(zt);
                        double tt;
                        tt=(N_wspc[k]+N_wspy[k]*(TOW-TOC)+N_wspysd[k]*pow((TOW-TOC),2))*c;
                        sat_clock.push_back(tt);//钟差改正

                        kjzz_ddzz(APPROX_X,APPROX_Y,APPROX_Z);
                        double xx,yy,zz;
                        xx=-sin(B)*cos(L)*(xt-APPROX_X)-sin(B)*sin(L)*(yt-APPROX_Y)+cos(B)*(zt-APPROX_Z);
                        yy=-sin(L)*(xt-APPROX_X)+cos(L)*(yt-APPROX_Y);
                        zz=cos(B)*cos(L)*(xt-APPROX_X)+cos(B)*sin(L)*(yt-APPROX_Y)+sin(B)*(zt-APPROX_Z);
                        double El,Az;
                        Az=atan2(yy,xx);//方位角
                        El=atan2(zz,sqrt(pow(xx,2)+pow(yy,2)));//高度角
                        Elevation.push_back(El);
                        Azimuth.push_back(Az);
                        double zz1,zz2;
                        zz1=PI/2-El;
                        zz2=Trop_delay(H,zz1);//对流层改正
                        Trop_Delay.push_back(zz2);
                        double pj=sqrt(pow(xt,2)+pow(yt,2)+pow(zt,2));
                        double pi=sqrt(pow(APPROX_X,2)+pow(APPROX_Y,2)+pow(APPROX_Z,2));
                        double pij=sqrt(pow((APPROX_X-xt),2)+pow((APPROX_Y-yt),2)+pow((APPROX_Z-zt),2));
                        double Relativity1=2*u_e*log((pj+pi+pij)/(pj+pi-pij))/(c*c);
                        double Relativity2=-2*sqrt(a_s*u_e)*N_e[k]*sin(E)/c;
                        Relativity.push_back(Relativity1+Relativity2);//相对论改正
                        double sagnac=-wie*((xt-APPROX_X)*yt-(yt-APPROX_Y)*xt)/c;//地球自转改正
                        Sagnac.push_back(sagnac);
                        break;
                    }
                }
            }
            if(bjz==1)
            {
                GPS_xt.push_back(0);
                GPS_yt.push_back(0);
                GPS_zt.push_back(0);
                sat_clock.push_back(0);
                Elevation.push_back(0);
                Azimuth.push_back(0);
                Trop_Delay.push_back(0);
                Relativity.push_back(0);
                Sagnac.push_back(0);
            }
            bjz=1;
        }
    }
    GPS_Xt.resize(O_year.size());
    GPS_Yt.resize(O_year.size());
    GPS_Zt.resize(O_year.size());//基准站X,Y,Z的坐标
    GPS_sat_clock.resize(O_year.size());//钟差改正
    GPS_Elevation.resize(O_year.size());//高度角
    GPS_Azimuth.resize(O_year.size());//方位角
    GPS_Trop_Delay.resize(O_year.size());//对流程改正
    GPS_Relativity.resize(O_year.size());//相对论改正
    GPS_Sagnac.resize(O_year.size());//地球自转改正
    int sum=0;
    for(unsigned int ll=0;ll<O_year.size();ll++)
    {
        if(ll>0)
            sum+=O_WSGS[ll-1];
        for(int kk=0;kk<O_WSGS[ll];kk++)
        {
            GPS_Xt[ll].push_back(GPS_xt[kk+sum]);
            GPS_Yt[ll].push_back(GPS_yt[kk+sum]);
            GPS_Zt[ll].push_back(GPS_zt[kk+sum]);
            GPS_sat_clock[ll].push_back(sat_clock[kk+sum]);
            GPS_Elevation[ll].push_back(Elevation[kk+sum]);
            GPS_Azimuth[ll].push_back(Azimuth[kk+sum]);
            GPS_Trop_Delay[ll].push_back(Trop_Delay[kk+sum]);
            GPS_Relativity[ll].push_back(Relativity[kk+sum]);
            GPS_Sagnac[ll].push_back(Sagnac[kk+sum]);

        }
    }
}



void gps::spp_out(string filename)
{
    ofstream outfile(filename,ios::out);
    outfile<<"PRN:  SatPosition(X)  SatPosition(Y)  SatPosition(Z)  Sat Clock(m)  Elevation(°)  Azimuth(°)   Trop Delay   Relativity(m)    Sagnac(m)           C1(m)         P2(m)        L1(cycles)     L2(cycles)"<<endl;


    for(unsigned int d=0;d<O_C1.size();d++)
    {
        for(unsigned int j=0;j<O_PRN[d].size();j++){
            outfile<<left<<setw(6)<<setiosflags(ios::fixed)<<setprecision(0)<<O_PRN[d][j];
            outfile<<left<<setw(16)<<setiosflags(ios::fixed)<<setprecision(4)<<setw(10);
            outfile<<left<<setw(16)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Xt[d][j];
            outfile<<left<<setw(16)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Yt[d][j];
            outfile<<left<<setw(16)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Zt[d][j];
            outfile<<left<<setw(16)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_sat_clock[d][j];
            outfile<<left<<setw(14)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Elevation[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Azimuth[d][j];
            outfile<<left<<setw(14)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Trop_Delay[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Relativity[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<GPS_Sagnac[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<O_C1[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<O_P2[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<O_L1[d][j];
            outfile<<left<<setw(15)<<setiosflags(ios::fixed)<<setprecision(4)<<O_L2[d][j]<<endl;

        }
        outfile<<endl;
    }
}
 void gps::Singledifference()
 {
    double f1=1575.42;
    double f2=1227.60;
    double wavelong=86.19;
    GPS_NF.resize(O_year.size());
    GPS_MWGCZ.resize(O_year.size());
    for(unsigned int i=0;i<O_year.size();i++)
    {
        for(unsigned int j=0;j<O_WSGS.size();j++)
        {
          double nf=((f1*(O_C1[i][j])+f2*(O_P2[i][j]))/(f1+f2)-(O_L1[i][j]-O_L2[i][j])*wavelong)/wavelong;
          GPS_NF[i].push_back(nf);
          double GCZ=wavelong*(O_C1[i][j]-O_L2[i][j]);
          double JSZ=sqrt((GPS_Xt[i][j]-APPROX_X)*(GPS_Xt[i][j]-APPROX_X)+(GPS_Yt[i][j]-APPROX_Y)*(GPS_Yt[i][j]-APPROX_Y)
                              +(GPS_Zt[i][j]-APPROX_Z)*(GPS_Zt[i][j]-APPROX_Z));

        }
    }
 }

void SCPC::Adjustment(gps mob, gps ref)
{

}
double gps::glg_gps(double Y,double M,double D,double UT1,double UT2,double UT3)
{
    double y,m,JD;
    double TOW,UT;
    UT=UT1+UT2/60+UT3/3600;
    if(M<=2)
    {
        y=Y-1;
        m=M+12;
        JD=floor(365.25*y)+floor(30.6001*(m+1))+D+UT/24+1720981.5;
    }
    else
    {
        y=Y;
        m=M;
        JD=floor(365.25*y)+floor(30.6001*(m+1))+D+UT/24+1720981.5;
    }
    TOW=fmod(JD-2444244.5,7)*86400;//格里高利转为GPS时
    return TOW;
}
void gps::kjzz_ddzz(double X, double Y, double Z)
{
    double B1;
    double N,cz;
    L=atan2(Y,X);
    B=atan2(Z,sqrt(X*X+Y*Y));
    do
    {
        N=aa/sqrt(1-e2*sin(B)*sin(B));
        H=Z/sin(B)-N*(1-e2);
        B1=B;
        B=atan2(Z*(N+H),sqrt(X*X+Y*Y)*(N*(1-e2)+H));
        cz=B-B1;
    }while(cz>1e-12);
}
double gps::Trop_delay(double H,double z)
{
    double Tropdelay;
    double H0=0,P0=1013.25,T0=18,Rh0=0.5,RE=6378137;
    double hd,hw =11000,rd,rw,ad,aw,bd,bw;
    double TropDelay_w,TropDelay_d,P,T,Rh,e,Nw,Nd;
    P=P0*pow((1-0.000226*(H-H0)),5.225);
    T=T0-0.0065*(H-H0)+273.16;
    Rh=Rh0*exp(-0.0006396*(H-H0));
    e=Rh*exp(-37.2465+0.213166*T-0.000256908*T*T);
    Nd=77.64*P/T;
    Nw=-12.96*e/T+371800*e/(T*T);
    hd=40136+148.72*(T-273.16);
    rd=sqrt((RE+hd)*(RE+hd)-RE*RE*sin(z)*sin(z))-RE*cos(z);
    rw=sqrt((RE+hw)*(RE+hw)-RE*RE*sin(z)*sin(z))-RE*cos(z);
    ad=-cos(z)/hd;
    aw=-cos(z)/hw;
    bd=-sin(z)*sin(z)/(2*hd*RE);
    bw=-sin(z)*sin(z)/(2*hw*RE);
    TropDelay_d=1e-6*Nd*(rd+4*ad*rd*rd/2+(6*ad*ad+4*bd)*rd*rd*rd/3+4*ad*(ad*ad+3*bd)*pow(rd,4)/4
                         +(pow(ad,4)+12*ad*ad*bd+6*bd*bd)*pow(rd,5)/5+4*ad*bd*(ad*ad+3*bd)*pow(rd,6)/6
                         +bd*bd*(6*ad*ad+4*bd)*pow(rd,7)/7+4*ad*bd*bd*bd*pow(rd,8)/8+pow(bd,4)*pow(rd,9)/9);
    TropDelay_w=1e-6*Nw*(rw+4*aw*rw*rw/2+(6*aw*aw+4*bw)*rw*rw*rw/3+4*aw*(aw*aw+3*bw)*pow(rw,4)/4
                         +(pow(aw,4)+12*aw*aw*bw+6*bw*bw)*pow(rw,5)/5+4*aw*bw*(aw*aw+3*bw)*pow(rw,6)/6
                         +bw*bw*(6*aw*aw+4*bw)*pow(rw,7)/7+4*aw*bw*bw*bw*pow(rw,8)/8+pow(bw,4)*pow(rw,9)/9);
    Tropdelay=TropDelay_d+TropDelay_w;
    return Tropdelay;
}
